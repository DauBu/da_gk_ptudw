var pagesearch=1;
  function loadpageserch()
  {
    var input=$("#search input").val();
    $.ajax({
              url:`https://api.themoviedb.org/3/search/multi/?api_key=fa231b98fa7022a1fc2af5390641cafa`,
              type:"GET",
              data:{query:input,page:pagesearch}
          })
          .then(function(result){
            hideloading();
            console.log(result);
            $(".main").empty();
            listresult=result.results;
            for (let i=0;i<listresult.length;i++)
            {
              
      
              if ($("select").val()==1)  
              {
              if (listresult[i].media_type=="movie")
                $(".main").append(` <div class="col-sm-6 card " style="width: 18rem;padding:0px" >
                                <div class="row ">
                                <img class="col-sm-6 card-img-top " src="https://image.tmdb.org/t/p/w500/${listresult[i].poster_path}" alt="Card image cap">
                                <div class="col-sm-6">
                                  <h5 class="card-title">${listresult[i].title}</h5>
                                  <p class="card-text">${listresult[i].overview}</p>
                                  <p style="color: chartreuse;">Rating: ${listresult[i].vote_average}</p>
                                  <p>Release: ${listresult[i].release_date}</p>
                                  <a  class="btn btn-success movie" href="./moviedetail.html?&${listresult[i].id}">Detail</a>
                                </div>
                              </div>
                      </div>`); 
              }
              else{
                if (listresult[i].media_type!="movie")
                {
                  $(".main").append(` <div class="col-sm-6 card " style="width: 18rem;padding:0px" >
                                <div class="row ">
                                <img class="col-sm-6 card-img-top " src="https://image.tmdb.org/t/p/w500/${listresult[i].profile_path}" alt="Card image cap">
                                <div class="col-sm-6">
                                  <h5 class="card-title">${listresult[i].name}</h5>
                                  <p class="card-text">Deparment: ${listresult[i].known_for_department}</p>
                                  <p style="color: chartreuse;">Rating: ${listresult[i].vote_average}</p>
                                  <a href="./actordetail.html?&${listresult[i].id}"><button  class="btn btn-success movie" >Detail</button><a>
                                </div>
                              </div>
                      </div>`); 
                }
              }
             
            }
            
          })
          displayloading();
  }
