
function addTrailer(trailer) {
    for (let i of trailer) {
        if (i.site == 'YouTube') {
            $('#trailer').append(`
            <iframe title="YouTube video player" class="youtube-player mx-auto" type="text/html" 
             width="640" height="390" src="http://www.youtube.com/embed/${i.key}"
             frameborder="0" allowFullScreen></iframe>
            `);
            break;
        }
        }
}